﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Ephisys.UltimateConsole {
	public class CommandArgumentParser {
		/// <summary>
		/// Parses incoming string arguments into an instance of <typeparamref name="T"/>
		/// </summary>
		/// <typeparam name="T">The argument type</typeparam>
		public static T Parse<T>(string[] args) {
			T result = Activator.CreateInstance<T>();
			foreach (PropertyInfo prop in typeof(T).GetProperties()) {
				CommandArgumentAttribute att = prop.GetCustomAttributes(typeof(CommandArgumentAttribute), true).Cast<CommandArgumentAttribute>().SingleOrDefault();
				if (att != null) {
					if (att.Order > args.Length - 1) {
						if (att.Required) {
							throw new CommandArgumentException("No value specified for " + prop.Name);
						}
					}
					else {
						string value = args[att.Order];
						if (att.Required && String.IsNullOrWhiteSpace(value)) {
							throw new CommandArgumentException("Empty value specified for required argument " + prop.Name);
						}
						if (!String.IsNullOrWhiteSpace(att.ValidationExpression)) {
							if (!Regex.IsMatch(value, att.ValidationExpression)) {
								throw new CommandArgumentException("Invalid format for " + prop.Name);
							}
						}
						if (!String.IsNullOrWhiteSpace(value)) {
							if (prop.PropertyType.IsEnum) {
								try {
									var enumValue = Enum.Parse(prop.PropertyType, value, true);
									prop.SetValue(result, enumValue, null);
								}
								catch {
									throw new CommandArgumentException("Invalid value specified for " + prop.Name);
								}
							}
							else {
								prop.SetValue(result, System.Convert.ChangeType(value, prop.PropertyType), null);
							}
						}
					}
				}
			}
			return result;
		}
	}
}
