﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Ephisys.UltimateConsole {
	public class Program {
		internal static void WriteConsole(string textToWrite, ConsoleColor consoleColor) {
			try {
				Console.ForegroundColor = consoleColor;
				Console.WriteLine(textToWrite);
			}
			finally {
				Console.ResetColor();
			}
		}

		public static int Main(string[] args) {
			try {
				Command cmd = CommandMapper.DefaultCommand;
				if (args.Length > 0) {
					cmd = CommandMapper.GetCommandFromAlias(args[0]);
					if (cmd == null) {
						WriteConsole(String.Format("ERROR: Invalid Command {0}.\n", args[0]), ConsoleColor.Red);
						cmd = CommandMapper.DefaultCommand;
					}
				}
				cmd.Container = new ConsoleCommandContainer();
				cmd.ExecuteCommand(args);
				return 0;
			}
			catch (CommandException cex) {
				WriteConsole(cex.Message, ConsoleColor.Red);
				return 1;
			}
			catch (Exception ex) {
				WriteConsole("Unhandled Error - " + ex, ConsoleColor.Red);
				WriteConsole(String.Format("Please run {0} help for more information", Assembly.GetEntryAssembly().GetName().Name), ConsoleColor.Gray);
				return 2;
			}
		}
	}
}
