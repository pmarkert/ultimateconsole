using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using System.Linq;
using System.Reflection;

namespace Ephisys.UltimateConsole {
	public class CommandMapper : IConfigurationSectionHandler {
		private static bool Configured = false;
		private static Dictionary<string, Command> commandMap;
		private static List<Command> allCommands;
		private static string defaultCommand;
		private static string commandNamespace;
		public static Assembly CommandAssembly;

		public static Command DefaultCommand {
			get {
				EnsureConfigured();
				return Activator.CreateInstance(GetCommandType(defaultCommand)) as Command;
			}
		}

		private static Type GetCommandType(string commandTypeName) {
			string typeName = QualifyName(commandTypeName);
			var helpType = Type.GetType(typeName);
			if (helpType == null) {
				throw new ApplicationException(String.Format("Command class - {0} could not be resolved.", commandTypeName));
			}
			return helpType;
		}

		private static string QualifyName(string command) {
			if (command.Contains(".")) {
				return command;
			}
			else {
				return String.Format(commandNamespace, command);
			}
		}

		public static Command GetCommandFromAlias(string CommandName) {
			EnsureConfigured();
			CommandName = CommandName.ToLower();
			if (commandMap.ContainsKey(CommandName)) {
				return commandMap[CommandName];
			}
			else {
				return null;
			}
		}

		private static void Configure() {
			initializeLists();
			ConfigurationManager.GetSection("CommandMapper"); // NOTE - Just calling GetSection has the side-effect of running the initialization code
			if (!Configured) {
				autoConfigure();
			}
			if (String.IsNullOrEmpty(defaultCommand)) {
				defaultCommand = "help";
			}
			Configured = true;
		}

		private static void autoConfigure() {
			if (CommandAssembly == null) {
				CommandAssembly = Assembly.GetEntryAssembly();
			}
			foreach (Type t in CommandAssembly.GetTypes().Where(candidateType => typeof(Command).IsAssignableFrom(candidateType) && !candidateType.IsAbstract)) {
				try {
					addCommand(Activator.CreateInstance(t) as Command);
				}
				catch (Exception ex) {
					throw new ConfigurationErrorsException(String.Format("Error trying to create instance of Command - {0}", t.FullName), ex);
				}
				if (!commandMap.ContainsKey("help")) {
					var cmdHelp = new Help();
					addCommand(cmdHelp);
					defaultCommand = cmdHelp.GetType().AssemblyQualifiedName;
				}
			}
			Configured = true;
		}

		public static string[] GetAliasesForCommand(Command cmdToSearch) {
			EnsureConfigured();
			List<string> aliases = new List<string>();
			aliases.Add(cmdToSearch.GetType().Name.ToLower());
			foreach (string alias in commandMap.Keys) {
				Command cmd = commandMap[alias];
				if (!aliases.Contains(alias) && cmdToSearch.GetType() == cmd.GetType()) {
					aliases.Add(alias);
				}
			}
			return aliases.ToArray();
		}

		private static void EnsureConfigured() {
			if (!Configured) {
				lock (typeof(CommandMapper)) {
					if (!Configured) {
						Configure();
					}
				}
			}
		}

		public static Command[] GetAvailableCommands() {
			EnsureConfigured();
			return allCommands.ToArray();
		}

		#region IConfigurationSectionHandler Members
		public object Create(object parent, object configContext, XmlNode section) {
			XmlElement nodSection = section as XmlElement;
			defaultCommand = nodSection.GetAttribute("DefaultCommand");
			commandNamespace = nodSection.GetAttribute("DefaultCommandNamespace");
			if (String.IsNullOrEmpty(commandNamespace)) {
				commandNamespace = typeof(CommandMapper).Namespace + ".Commands";
			}
			foreach (XmlElement nodCommand in section.SelectNodes("Command")) {
				Command cmd = Activator.CreateInstance(GetCommandType(nodCommand.GetAttribute("Type"))) as Command;
				if (cmd != null) {
					cmd.Initialize(nodCommand);
					addCommand(cmd);
					string aliases = nodCommand.GetAttribute("Aliases");
					if (!String.IsNullOrWhiteSpace(aliases)) {
						foreach (string alias in aliases.Split(new char[] { ',', ';', ' ' })) {
							if (commandMap.ContainsKey(alias.ToLower())) {
								throw new ConfigurationErrorsException("CommandMap already contains an entry for " + alias);
							}
							addAliasForCommand(alias, cmd);
						}
					}
				}
			}
			Configured = true;
			return null;
		}

		private static void addAliasForCommand(string alias, Command cmd) {
			commandMap.Add(alias.ToLower(), cmd);
		}

		private static void addCommand(Command cmd) {
			allCommands.Add(cmd);
			addAliasForCommand(fixCommandName(cmd).ToLower(), cmd);
		}

		internal static string fixCommandName(Command cmd) {
			string cmdName = cmd.GetType().Name;
			const string COMMAND = "command";
			if (cmdName.ToLower().EndsWith(COMMAND)) {
				cmdName = cmdName.Substring(0, cmdName.Length - COMMAND.Length);
			}
			return cmdName;
		}

		private static void initializeLists() {
			commandMap = new Dictionary<string, Command>();
			allCommands = new List<Command>();
		}
		#endregion
	}
}
