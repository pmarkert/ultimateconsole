﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ephisys.UltimateConsole {
	public interface ICommandContainer {
		void WriteConsole(string textToWrite);
		void WriteConsole(string textToWrite, ConsoleColor consoleColor);
	}
}
