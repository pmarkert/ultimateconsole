using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace Ephisys.UltimateConsole {
    public class HelpDisplayer {
        private static Regex reColors = new Regex(@"\{(?<Color>/?\w+)\}", RegexOptions.Multiline);

        private const string NoContent = "{RED}Help Topic not found.{/RED}";

        public static void DisplayHelpContent(string ContentToShow) {
            if (ContentToShow == null) {
                ContentToShow = NoContent;
            }
            Stack<ConsoleColor> colors = new Stack<ConsoleColor>();
            try {
                MatchCollection m = reColors.Matches(ContentToShow);
                int index = 0;
                for(int i=0;i<m.Count;i++) {
                    Console.Write(ContentToShow.Substring(index, m[i].Index-index));
                    if (m[i].Groups["Color"].Value[0] == '/') {
                        if (colors.Count > 0) {
                            Console.ForegroundColor = colors.Pop();
                        }
                        else {
                            Console.ResetColor();
                        }
                    }
                    else {
                        try {
                            colors.Push(Console.ForegroundColor);
                            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), m[i].Groups["Color"].Value, true);
                        }
                        catch (Exception) {
                            // noop to swallow
                        }
                    }
                    index = m[i].Index + m[i].Length;
                }
                if (index < ContentToShow.Length) {
                    Console.Write(ContentToShow.Substring(index));
                }
            }
            finally {
                Console.ResetColor();
            }
            Console.WriteLine();
        }
    }
}
