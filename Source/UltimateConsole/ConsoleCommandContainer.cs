﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ephisys.UltimateConsole {
	public class ConsoleCommandContainer : ICommandContainer {
		public void WriteConsole(string textToWrite) {
			Program.WriteConsole(textToWrite, ConsoleColor.Gray);
		}

		public void WriteConsole(string textToWrite, ConsoleColor consoleColor) {
			Program.WriteConsole(textToWrite, consoleColor);
		}
	}
}
