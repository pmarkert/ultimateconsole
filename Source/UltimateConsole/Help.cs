﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ephisys.UltimateConsole {
	class Help : Command {
		public override string CommandDescription {
			get { return "Displays this help file"; }
		}

		protected override void ProcessCommand(string[] args) {
			this.args = args;
			ShowHelp();
		}

		protected internal override void ShowHelp() {
			if (args != null && args.Length >= 2) {
				var cmd = CommandMapper.GetCommandFromAlias(args[1]);
				ShowHelpForCommand(cmd);
			}
			else {
				ShowDefaultHelpContent();
			}
		}

		private void ShowHelpForCommand(Command cmd) {
			if (cmd != null) { // Could be a non-command help topic
				// Show help for the specified command
				HelpDisplayer.DisplayHelpContent(cmd.HelpText);
				HelpDisplayer.DisplayHelpContent(GetAliasesForCommand(cmd));
			}
			else {
				HelpDisplayer.DisplayHelpContent("{RED}Help Topic not found{/RED}");
			}
		}

		private string GetAliasesForCommand(Command cmd) {
			string[] aliases = CommandMapper.GetAliasesForCommand(cmd);
			if(aliases.Length>0) {
				return String.Format(" ALIASES\n\n   " + String.Join(",", aliases));
			}
			return String.Empty;
		}

		private void ShowDefaultHelpContent() {
			HelpDisplayer.DisplayHelpContent(HelpText);
			var sb = new StringBuilder("\n List of Available Commands:\n");
			foreach (var cmd in CommandMapper.GetAvailableCommands()) {
				sb.AppendLine("   {CYAN}" + CommandMapper.fixCommandName(cmd) + "{/CYAN}: " + cmd.CommandDescription);
			}
			HelpDisplayer.DisplayHelpContent(sb.ToString());
		}
	}
}
