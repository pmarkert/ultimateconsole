﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Ephisys.UltimateConsole {
	public abstract class Command<T> : Command {
		public abstract void ProcessCommand(T args);

		protected override void ProcessCommand(string[] args) {
			ProcessCommand(CommandArgumentParser.Parse<T>(args));
		}

		protected override string FormatDefaultHelptext() {
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(base.FormatDefaultHelptext());
			sb.AppendLine(" ARGUMENTS");
			Dictionary<CommandArgumentAttribute, PropertyInfo> arguments = new Dictionary<CommandArgumentAttribute, PropertyInfo>();
			foreach (PropertyInfo prop in typeof(T).GetProperties()) {
				CommandArgumentAttribute att = prop.GetCustomAttributes(typeof(CommandArgumentAttribute), true).Cast<CommandArgumentAttribute>().FirstOrDefault();
				if (att != null) {
					arguments.Add(att, prop);
				}
			}
			foreach (var att in arguments.Keys.OrderBy(a => a.Order)) {
				sb.Append("  " + (att.Required ? "{RED}*{/RED}" : "") + "{WHITE}" + arguments[att].Name + "{/WHITE} :");
				if (!String.IsNullOrWhiteSpace(att.Description)) {
					sb.Append(" " + att.Description);
				}
				if (arguments[att].PropertyType.IsEnum) {
					sb.AppendLine(" {DARKYELLOW}(" + String.Join(" | ", Enum.GetNames(arguments[att].PropertyType)) + "){/DARKYELLOW}");
				}
				sb.AppendLine();
			}
			sb.AppendLine();
			sb.AppendLine("  {RED}*{/RED} Indicates a required parameter.");
			return sb.ToString();
		}
	}
}
