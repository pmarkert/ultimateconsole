﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Ephisys.UltimateConsole {
	public class CommandException : Exception {
		public CommandException() {
		}

		public CommandException(string message)
			: base(message) {
		}

		public CommandException(string message, Exception innerException)
			: base(message, innerException) {
		}

		protected CommandException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context) {
		}
	}
}
