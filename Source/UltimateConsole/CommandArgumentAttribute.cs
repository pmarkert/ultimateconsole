﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ephisys.UltimateConsole {
	[AttributeUsage(AttributeTargets.Property)]
	public class CommandArgumentAttribute : Attribute {
		public CommandArgumentAttribute(int order, bool required) : this(order, required, null, null) {
		}

		public CommandArgumentAttribute(int order, bool required, string description) : this(order, required, description, null) {
		}

		public CommandArgumentAttribute(int order, bool required, string description, string validationExpression) {
			this.Order = order;
			this.Required = required;
			this.Description = description;
			this.ValidationExpression = validationExpression;
		}

		public int Order {
			get;
			set;
		}

		public string Description {
			get;
			set;
		}

		public bool Required {
			get;
			set;
		}

		public string ValidationExpression {
			get;
			set;
		}
	}
}
