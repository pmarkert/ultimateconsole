using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;

namespace Ephisys.UltimateConsole {
	public abstract class Command {
		protected string[] args;

		public abstract string CommandDescription { get; }

		public ICommandContainer Container { get; set; }

		public virtual string HelpText {
			get {
				var stream = GetType().Assembly.GetManifestResourceStream(GetType().FullName + ".txt");
				if (stream != null) {
					using (StreamReader sr = new StreamReader(stream)) {
						return sr.ReadToEnd();
					}
				}
				else {
					return FormatDefaultHelptext();
				}
			}
		}

		protected virtual string FormatDefaultHelptext() {
			StringBuilder sb = new StringBuilder();
			sb.Append(" The {CYAN}");
			sb.Append(GetType().Name);
			sb.AppendLine("{/CYAN} command:");
			sb.AppendLine(" " + CommandDescription);
			return sb.ToString();
		}

		public void ExecuteCommand(string[] args) {
			try {
				ProcessCommand(args);
			}
			catch (CommandArgumentException ex) {
				WriteConsole("Error parsing arguments - " + ex.Message, ConsoleColor.Red);
				ShowHelp();
			}
			catch (Exception ex) {
				WriteConsole("Unhandled exception - " + ex.ToString(), ConsoleColor.Red);
				ShowHelp();
			}
		}

		protected abstract void ProcessCommand(string[] args);

		protected internal void WriteConsole(string textToWrite) {
			Container.WriteConsole(textToWrite, ConsoleColor.Gray);
		}

		protected internal void WriteConsole(string textToWrite, ConsoleColor consoleColor) {
			Container.WriteConsole(textToWrite, consoleColor);
		}

		protected internal virtual void ShowHelp() {
			HelpDisplayer.DisplayHelpContent(HelpText);
			HelpDisplayer.DisplayHelpContent("\n COMMAND ALIASES:\n    {CYAN}" + String.Join("{/CYAN}, {CYAN}", CommandMapper.GetAliasesForCommand(this)) + "{/CYAN}");
		}

		/// <summary>
		/// Override this method in a sub-class to read custom configuration information for the command.
		/// </summary>
		/// <param name="nodCommand">The XmlElement for the Command as read from the application configuration file</param>
		protected internal virtual void Initialize(System.Xml.XmlElement nodCommand) {
			// NOOP - Nothing to do in the base implementation, but sub-classes can override and read optional configuration information from the node if needed
		}
	}
}
