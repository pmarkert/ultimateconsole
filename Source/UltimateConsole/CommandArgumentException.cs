﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ephisys.UltimateConsole {
	public class CommandArgumentException : ApplicationException {
		public CommandArgumentException(string ArgumentName)
			: base(ArgumentName) {
		}
	}
}
